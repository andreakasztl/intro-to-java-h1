package main.java.ex3;

public class Number {
    public static int maxDigit(int n){
        int digit;
        int max = 0;
        do {
            digit = n%10;
            n = n/10;
            max = Math.max(digit, max);
        }
        while (n > 0);

        return max;
    }
}
