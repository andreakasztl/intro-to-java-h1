package main.java.ex4;

public class Main {

    public static void main(String[] args) {
        int number = 7889887;
        boolean isPal = Palindrome.isPalindrome(number);

        if (isPal)
            System.out.println(number + " is a palindrome.");
        else
            System.out.println(number + " is not a palindrome.");
    }
}
