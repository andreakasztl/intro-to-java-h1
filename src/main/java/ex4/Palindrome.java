package main.java.ex4;

public class Palindrome {

    public static boolean isPalindrome (int n){
        int reversed = 0, r, origN;
        origN = n;

        while (n>0){
            r = n%10;
            reversed = reversed*10 + r;
            n = n/10;
        }
        return reversed == origN;

    }
}
