package main.java.ex2;

import com.sun.javaws.exceptions.InvalidArgumentException;

public class Number {
    public static double minFromArray(double[] nums){

        int length = nums.length;
        double min;

        if (length > 0)
            min = nums[0];
        else throw new IllegalArgumentException("empty array");

        for (int i = 1 ; i < length ; i++){
           if (nums[i] < min){
               min = nums[i];
           }
        }

        return  min;

    }
}
