package main.java.ex5;

import java.util.ArrayList;

public class Prime {
    public static ArrayList<Integer> allPrimesUnder (int max){

        Boolean[] list = new Boolean[max];
        ArrayList res = new ArrayList();
        for (int i=2; i<max; i++){
            list[i]= true;
        }

        for (int i=2; i<=Math.sqrt(max); i++){
            if (list[i]){
                for (int j = i*i; j < max; j+=i){
                    list[j] = false;
                }
            }
        }

        for (int i=2; i<max; i++){
            if (list[i])
                res.add(i);
        }
        return res;

    }


}
